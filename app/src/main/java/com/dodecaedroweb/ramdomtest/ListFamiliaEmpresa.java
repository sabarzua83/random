package com.dodecaedroweb.ramdomtest;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dodecaedroweb.ramdomtest.R;

public class ListFamiliaEmpresa extends Activity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter aFamiliaEmpresa;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_familia_empresa);
        recyclerView = (RecyclerView)  findViewById (R.id.list_familia_empresa);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(aFamiliaEmpresa);

    }
}
