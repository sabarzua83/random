package com.dodecaedroweb.ramdomtest.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuperFamilia {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("idEmpresa")
    @Expose
    private Integer idEmpresa;
    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("createdAt")
    @Expose
    private Object createdAt;
    @SerializedName("updatedAt")
    @Expose
    private Object updatedAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public SuperFamilia() {
    }

    /**
     *
     * @param updatedAt
     * @param nombre
     * @param codigo
     * @param id
     * @param idEmpresa
     * @param createdAt
     */
    public SuperFamilia(Integer id, Integer idEmpresa, String codigo, String nombre, Object createdAt, Object updatedAt) {
        super();
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.codigo = codigo;
        this.nombre = nombre;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}