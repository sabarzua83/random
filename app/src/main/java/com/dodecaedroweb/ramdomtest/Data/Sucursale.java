
package com.dodecaedroweb.ramdomtest.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sucursale {

    @SerializedName("nombreEmpresa")
    @Expose
    private String nombreEmpresa;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("idEmpresa")
    @Expose
    private Integer idEmpresa;
    @SerializedName("idCliente")
    @Expose
    private Integer idCliente;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("idVendedor")
    @Expose
    private Object idVendedor;
    @SerializedName("codigoClienteEnEmpresa")
    @Expose
    private String codigoClienteEnEmpresa;
    @SerializedName("nombreSucursalEnEmpresa")
    @Expose
    private String nombreSucursalEnEmpresa;
    @SerializedName("sucursal")
    @Expose
    private String sucursal;
    @SerializedName("latitud")
    @Expose
    private Object latitud;
    @SerializedName("longitud")
    @Expose
    private Object longitud;
    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("ciudad")
    @Expose
    private String ciudad;
    @SerializedName("comuna")
    @Expose
    private String comuna;
    @SerializedName("telefono")
    @Expose
    private String telefono;
    @SerializedName("rutaDias")
    @Expose
    private String rutaDias;
    @SerializedName("diaCobra")
    @Expose
    private String diaCobra;
    @SerializedName("zona")
    @Expose
    private String zona;
    @SerializedName("condicionesDePago")
    @Expose
    private String condicionesDePago;
    @SerializedName("autorizadoParaCompra")
    @Expose
    private Object autorizadoParaCompra;
    @SerializedName("createdAt")
    @Expose
    private Object createdAt;
    @SerializedName("updatedAt")
    @Expose
    private Object updatedAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public Sucursale() {
    }

    /**
     *
     * @param nombreEmpresa
     * @param idVendedor
     * @param sucursal
     * @param condicionesDePago
     * @param direccion
     * @param ciudad
     * @param diaCobra
     * @param idCliente
     * @param telefono
     * @param rutaDias
     * @param updatedAt
     * @param id
     * @param comuna
     * @param autorizadoParaCompra
     * @param idEmpresa
     * @param codigoClienteEnEmpresa
     * @param createdAt
     * @param nombreSucursalEnEmpresa
     * @param latitud
     * @param zona
     * @param longitud
     * @param createDate
     */
    public Sucursale(String nombreEmpresa, Integer id, Integer idEmpresa, Integer idCliente, String createDate, Object idVendedor, String codigoClienteEnEmpresa, String nombreSucursalEnEmpresa, String sucursal, Object latitud, Object longitud, String direccion, String ciudad, String comuna, String telefono, String rutaDias, String diaCobra, String zona, String condicionesDePago, Object autorizadoParaCompra, Object createdAt, Object updatedAt) {
        super();
        this.nombreEmpresa = nombreEmpresa;
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.idCliente = idCliente;
        this.createDate = createDate;
        this.idVendedor = idVendedor;
        this.codigoClienteEnEmpresa = codigoClienteEnEmpresa;
        this.nombreSucursalEnEmpresa = nombreSucursalEnEmpresa;
        this.sucursal = sucursal;
        this.latitud = latitud;
        this.longitud = longitud;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.comuna = comuna;
        this.telefono = telefono;
        this.rutaDias = rutaDias;
        this.diaCobra = diaCobra;
        this.zona = zona;
        this.condicionesDePago = condicionesDePago;
        this.autorizadoParaCompra = autorizadoParaCompra;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Object getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Object idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getCodigoClienteEnEmpresa() {
        return codigoClienteEnEmpresa;
    }

    public void setCodigoClienteEnEmpresa(String codigoClienteEnEmpresa) {
        this.codigoClienteEnEmpresa = codigoClienteEnEmpresa;
    }

    public String getNombreSucursalEnEmpresa() {
        return nombreSucursalEnEmpresa;
    }

    public void setNombreSucursalEnEmpresa(String nombreSucursalEnEmpresa) {
        this.nombreSucursalEnEmpresa = nombreSucursalEnEmpresa;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public Object getLatitud() {
        return latitud;
    }

    public void setLatitud(Object latitud) {
        this.latitud = latitud;
    }

    public Object getLongitud() {
        return longitud;
    }

    public void setLongitud(Object longitud) {
        this.longitud = longitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRutaDias() {
        return rutaDias;
    }

    public void setRutaDias(String rutaDias) {
        this.rutaDias = rutaDias;
    }

    public String getDiaCobra() {
        return diaCobra;
    }

    public void setDiaCobra(String diaCobra) {
        this.diaCobra = diaCobra;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getCondicionesDePago() {
        return condicionesDePago;
    }

    public void setCondicionesDePago(String condicionesDePago) {
        this.condicionesDePago = condicionesDePago;
    }

    public Object getAutorizadoParaCompra() {
        return autorizadoParaCompra;
    }

    public void setAutorizadoParaCompra(Object autorizadoParaCompra) {
        this.autorizadoParaCompra = autorizadoParaCompra;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
