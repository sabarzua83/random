package com.dodecaedroweb.ramdomtest.Data;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataFamilia {

    @SerializedName("idEmpresa")
    @Expose
    private String idEmpresa;
    @SerializedName("familias")
    @Expose
    private List<Familia> familias = null;
    @SerializedName("superFamilias")
    @Expose
    private List<SuperFamilia> superFamilias = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public DataFamilia() {
    }

    /**
     *
     * @param familias
     * @param idEmpresa
     * @param superFamilias
     */
    public DataFamilia(String idEmpresa, List<Familia> familias, List<SuperFamilia> superFamilias) {
        super();
        this.idEmpresa = idEmpresa;
        this.familias = familias;
        this.superFamilias = superFamilias;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public List<Familia> getFamilias() {
        return familias;
    }

    public void setFamilias(List<Familia> familias) {
        this.familias = familias;
    }

    public List<SuperFamilia> getSuperFamilias() {
        return superFamilias;
    }

    public void setSuperFamilias(List<SuperFamilia> superFamilias) {
        this.superFamilias = superFamilias;
    }

}