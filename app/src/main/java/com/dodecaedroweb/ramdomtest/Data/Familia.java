package com.dodecaedroweb.ramdomtest.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Familia {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("idEmpresa")
    @Expose
    private Integer idEmpresa;
    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("updatedAt")
    @Expose
    private Object updatedAt;
    @SerializedName("createdAt")
    @Expose
    private Object createdAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public Familia() {
    }

    /**
     *
     * @param updatedAt
     * @param nombre
     * @param codigo
     * @param id
     * @param idEmpresa
     * @param createdAt
     */
    public Familia(Integer id, Integer idEmpresa, String codigo, String nombre, Object updatedAt, Object createdAt) {
        super();
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.codigo = codigo;
        this.nombre = nombre;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

}