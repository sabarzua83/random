package com.dodecaedroweb.ramdomtest.Data;

import com.dodecaedroweb.ramdomtest.Data.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FemiliaEmpresas {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("errorMsg")
    @Expose
    private String errorMsg;
    @SerializedName("data")
    @Expose
    private DataFamilia dataFamilia;

    /**
     * No args constructor for use in serialization
     *
     */
    public FemiliaEmpresas() {
    }

    /**
     *
     * @param dataFamiia
     * @param errorCode
     * @param errorMsg
     * @param success
     */
    public FemiliaEmpresas(Boolean success, Integer errorCode, String errorMsg, Data data) {
        super();
        this.success = success;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.dataFamilia = dataFamilia;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public DataFamilia getDataFamilia() {
        return dataFamilia;
    }

    public void setDataFamilia(DataFamilia dataFamilia) {
        this.dataFamilia = dataFamilia;
    }

}
