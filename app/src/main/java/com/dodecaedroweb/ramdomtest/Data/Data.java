package com.dodecaedroweb.ramdomtest.Data;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("sucursales")
    @Expose
    private List<Sucursale> sucursales = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param sucursales
     * @param token
     */
    public Data(String token, List<Sucursale> sucursales) {
        super();
        this.token = token;
        this.sucursales = sucursales;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Sucursale> getSucursales() {
        return sucursales;
    }

    public void setSucursales(List<Sucursale> sucursales) {
        this.sucursales = sucursales;
    }

}