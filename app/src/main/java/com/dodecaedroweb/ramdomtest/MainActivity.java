package com.dodecaedroweb.ramdomtest;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dodecaedroweb.ramdomtest.Data.LoginResponse;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private EditText userLogin;
    private EditText passLogin;
    private Button btnLogin;
    private String token;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivity.context = getApplicationContext();
        userLogin = (EditText) findViewById(R.id.txt_user_name);
        passLogin = (EditText) findViewById(R.id.txt_user_pass);
        Button btnLogin = (Button) findViewById(R.id.tbnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                funGetToker();
            }
        });
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private void funGetToker(){
        String user = String.valueOf(R.id.txt_user_name);
        String pass = String.valueOf(R.id.txt_user_pass);
        Call<LoginResponse> call = RamdomAPIAdapter.getApiServices().getToken(user, pass);
        call.enqueue(new RandomCallBack());
    }

    public class RandomCallBack implements Callback<LoginResponse> {

        @Override
        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
            if (response.isSuccessful()){
                LoginResponse loginResponse = response.body();

            }else {
                Toast.makeText(getContext(), "Error en la respuesta !!!",Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<LoginResponse> call, Throwable t) {
            Toast.makeText(getContext(), "fallo de comunicacion !!!",Toast.LENGTH_LONG).show();

        }
    }

    public static Context getContext() {
        return MainActivity.context;
    }

}
