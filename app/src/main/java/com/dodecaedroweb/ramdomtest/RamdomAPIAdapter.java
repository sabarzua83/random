package com.dodecaedroweb.ramdomtest;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RamdomAPIAdapter{

    private static RandomServices API_RANDOM;

    public static RandomServices getApiServices(){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        String baseURL = "http://movil.random.cl:8081/";

        if (API_RANDOM == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
            API_RANDOM = retrofit.create(RandomServices.class);

        }
        return API_RANDOM;
    }
}
