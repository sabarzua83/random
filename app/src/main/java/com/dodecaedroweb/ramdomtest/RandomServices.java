package com.dodecaedroweb.ramdomtest;

import com.dodecaedroweb.ramdomtest.Data.LoginResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RandomServices {

    @FormUrlEncoded
    @GET("login")
    Call<LoginResponse> getToken(@Path("user") String user, @Path("pass") String pass);


}
